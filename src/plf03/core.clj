(ns plf03.core)

;;comp
(defn función-comp-1
  []
  (let [f (fn [x] (* 20 x))
        g (fn [x] (/ x 100))
        z (comp g f)]
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x y z] (vector x y z))
        g (fn [x] (filter odd? x))
        h (fn [x] (map char x))
        z (comp h g f)]
    (z 65 66 67)))

(defn función-comp-3
  []
  (let [f (fn [] (range 0 21))
        g (fn [x] (take 8 x))
        h (fn [x] (map inc x))
        z (comp h h g f)]
    (z)))

(defn función-comp-4
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (repeat 3 x))
        h (fn [x] (flatten x))
        i (fn [x] (apply str x))
        z (comp i h g f)]
    (z "hola")))

(defn función-comp-5
  []
  (let [f (fn [] (range 65 75))
        g (fn [x] (filter odd? x))
        h (fn [x] (map char x))
        i (fn [x] (apply str x))
        z (comp i h g f)]
    (z)))

(defn función-comp-6
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        z (comp g f g f f)]
    (z 10)))

(defn función-comp-7
  []
  (let [f (fn [] (range 10))
        g (fn [x] (filter even? x))
        h (fn [x] (into #{} x))
        z (comp h g f)]
    (z)))

(defn función-comp-8
  []
  (let [f (fn [x y] (+ x y))
        g (fn [x] (* x x))
        h (fn [x] (/ x 100))
        i (fn [x] (double x))
        z (comp i h g f)]
    (z 10 23)))

(defn función-comp-9
  []
  (let [f (fn [x] (reverse x))
        g (fn [x] (apply str x))
        z (comp g f)]
    (z "Alejandra")))

(defn función-comp-10
  []
  (let [f (fn [x] (str x))
        g (fn [x] (count x))
        h (fn [x] (repeat (- x 1) 0))
        i (fn [x] (apply str "Su número es múltiplo de 1" x))
        j (fn [x] (flatten x))
        z (comp i j h g f)]
    (z 19293)))

(defn función-comp-11
  []
  (let [f (fn [x] (+ x 10))
        g (fn [x] (* x 10))
        h (fn [x] (- x 10))
        i (fn [x] (/ x 10))
        z (comp i h g f)]
    (z 921)))

(defn función-comp-12
  []
  (let [f (fn [x] (+ 65 x))
        g (fn [x] (char x))
        h (fn [x] (str x))
        z (comp h g f)]
    (z 19)))

(defn función-comp-13
  []
  (let [f (fn [a b c d e i] (vector a b c d e i))
        g (fn [x] (map char x))
        h (fn [x] (apply str x))
        z (comp h g f)]
    (z 89 65 90 77 73 78)))

(defn función-comp-14
  []
  (let [f (fn [] (range 67 123))
        g (fn [x] (into #{} x))
        h (fn [x] (take-last 10 x))
        i (fn [x] (sort x))
        z (comp i h g f)]
    (z)))

(defn función-comp-15
  []
  (let [f (fn [] (vector (rand-int 100) (rand-int 100)))
        g (fn [x] (range (apply min x) (apply max x)))
        z (comp g f)]
    (z)))

(defn función-comp-16
  []
  (let [f (fn [x y] (max x y))
        g (fn [x] (range x))
        h (fn [x] (take-last 10 x))
        i (fn [x] (reductions + x))
        z (comp i h g f)]
    (z 19 31)))

(defn función-comp-17
  []
  (let [f (fn [x y] (range x y))
        g (fn [x] (into #{} x))
        h (fn [x] (map neg? x))
        z (comp h g f)]
    (z -9 2)))

(defn función-comp-18
  []
  (let [f (fn [x] (map char x))
        g (fn [x] (map int x))
        h (fn [x] (filter (fn [y] (and (>= y 65) (<= y 90))) x))
        i (fn [x] (map char x))
        j (fn [x] (apply str "Las letras mayúsculas son: " x))
        z (comp j i h g f)]
    (z "JSBybdYW")))

(defn función-comp-19
  []
  (let [f (fn [x] (map int x))
        g (fn [x] (filter (fn [y] (and (>= y 97) (<= y 122))) x))
        h (fn [x] (map char x))
        i (fn [x] (apply str "Las letras minúsculas son: " x))
        z (comp i h g f)]
    (z "ANSDnsABsh")))

(defn función-comp-20
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 100 x))
        z (comp f g f)]
    (z 12)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

;;complement
(defn función-complement-1
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 12.3)))

(defn función-complement-2
  []
  (let [f (fn [x] (pos-int? x))
        z (complement f)]
    (z -19)))

(defn función-complement-3
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z false)))

(defn función-complement-4
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "Hola")))

(defn función-complement-5
  []
  (let [f (fn [x] (list? x))
        z (complement f)]
    (z [1 2 3])))

(defn función-complement-6
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z {92 3 1 3})))

(defn función-complement-7
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z '((hash-map :c 1 :f 2) [1 4 5]))))

(defn función-complement-8
  []
  (let [f (fn [x] (double? x))
        z (complement f)]
    (z 32/2)))

(defn función-complement-9
  []
  (let [f (fn [x] (symbol? x))
        z (complement f)]
    (z :n)))

(defn función-complement-10
  []
  (let [f (fn [x] (ratio? x))
        z (complement f)]
    (z "uno")))

(defn función-complement-11
  []
  (let [f (fn [x] (ident? x))
        z (complement f)]
    (z :x)))

(defn función-complement-12
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z :+)))

(defn función-complement-13
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z (< 3 4))))

(defn función-complement-14
  []
  (let [f (fn [x] (nil? x))
        z (complement f)]
    (z [])))

(defn función-complement-15
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z (= 2 3))))

(defn función-complement-16
  []
  (let [f (fn [x] (neg-int? x))
        z (complement f)]
    (z 123)))

(defn función-complement-17
  []
  (let [f (fn [x] (zero? x))
        z (complement f)]
    (z 0)))

(defn función-complement-18
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z '())))

(defn función-complement-19
  []
  (let [f (fn [x] (vector? x))
        z (complement f)]
    (z :x)))

(defn función-complement-20
  []
  (let [f (fn [x] (seq? x))
        z (complement f)]
    (z '(2 3 4))))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

;;constantly
(defn función-constantly-1
  []
  (let [x "Alejandra"
        z (constantly x)]
    (z [1 2 3])))

(defn función-constantly-2
  []
  (let [x 12
        z (constantly x)]
    (z '(1 2 3))))

(defn función-constantly-3
  []
  (let [x {"a" "b"}
        z (constantly x)]
    (z "Número")))

(defn función-constantly-4
  []
  (let [x #{:1 "Uno" :2 "Dos"}
        z (constantly x)]
    (z '(10 20))))

(defn función-constantly-5
  []
  (let [x \v
        z (constantly x)]
    (z "Hola")))

(defn función-constantly-6
  []
  (let [x ['() {} #{}]
        z (constantly x)]
    (z 9)))

(defn función-constantly-7
  []
  (let [x {\a \b}
        z (constantly x)]
    (z #{:a "A"})))

(defn función-constantly-8
  []
  (let [x (+ 3 4)
        z (constantly x)]
    (z "abecedario")))

(defn función-constantly-9
  []
  (let [x 190
        z (constantly x)]
    (z (* 8 10))))

(defn función-constantly-10
  []
  (let [x (< 3 10)
        z (constantly x)]
    (z (dec 1))))

(defn función-constantly-11
  []
  (let [x (dec 80)
        z (constantly x)]
    (z 34.1)))

(defn función-constantly-12
  []
  (let [x 51.01
        z (constantly x)]
    (z false)))

(defn función-constantly-13
  []
  (let [x false
        z (constantly x)]
    (z true)))

(defn función-constantly-14
  []
  (let [x (first [9 2 3])
        z (constantly x)]
    (z (last [9 2 3]))))

(defn función-constantly-15
  []
  (let [x (rand 100)
        z (constantly x)]
    (z "PLF")))

(defn función-constantly-16
  []
  (let [x (range 10 20)
        z (constantly x)]
    (z #{:Nombre "Alejandra"})))

(defn función-constantly-17
  []
  (let [x (vector "Hola")
        z (constantly x)]
    (z (list 1 2 3 4))))

(defn función-constantly-18
  []
  (let [x (char 87)
        z (constantly x)]
    (z 89/6)))

(defn función-constantly-19
  []
  (let [x (int \a)
        z (constantly x)]
    (z "Yazmin")))

(defn función-constantly-20
  []
  (let [x (= 3 4)
        z (constantly x)]
    (z 90)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

;;every-pred
(defn función-every-pred-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (rational? x))
        z (every-pred f g)]
    (z 12/5)))

(defn función-every-pred-2
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (number? x))
        h (fn [x] (> x 10))
        z (every-pred f g h)]
    (z 1023)))

(defn función-every-pred-3
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (ratio? x))
        z (every-pred f g)]
    (z 3/4 1/2)))

(defn función-every-pred-4
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (associative? x))
        z (every-pred f g)]
    (z #{:a "A"})))

(defn función-every-pred-5
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (empty? x))
        h (fn [x] (coll? x))
        z (every-pred f g h)]
    (z {})))

(defn función-every-pred-6
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (distinct? x))
        z (every-pred f g)]
    (z [1 2 3 4])))

(defn función-every-pred-7
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (every? pos-int? x))
        z (every-pred f g)]
    (z '(1 42 41 32 9.0))))

(defn función-every-pred-8
  []
  (let [f (fn [x] (every? number? x))
        g (fn [x] (every? (fn [x] (< x 10)) x))
        z (every-pred f g)]
    (z (list 9 2 6 2 3))))

(defn función-every-pred-9
  []
  (let [f (fn [x] (every? string? x))
        g (fn [x] (coll? x))
        z (every-pred f g)]
    (z '("uno" "dos" 3))))

(defn función-every-pred-10
  []
  (let [f (fn [x] (every? boolean? x))
        g (fn [x] (every? true? x))
        h (fn [x] (coll? x))
        z (every-pred f g h)]
    (z [(< 6 9) (= 12 12) (> 9 2) true])))

(defn función-every-pred-11
  []
  (let [f (fn [x] (= 6 (count x)))
        g (fn [x] (string? x))
        h (fn [x] (= \Y (first x)))
        z (every-pred f g h)]
    (z "Yazmin")))

(defn función-every-pred-12
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (> x 10))
        h (fn [x] (< x 20))
        z (every-pred f g h)]
    (z 18.45)))

(defn función-every-pred-13
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (pos? x))
        z (every-pred f g)]
    (z 120)))

(defn función-every-pred-14
  []
  (let [f (fn [x] (zero? x))
        g (fn [x] (int? x))
        z (every-pred f g)]
    (z 0)))

(defn función-every-pred-15
  []
  (let [f (fn [x] (ratio? x))
        g (fn [x] (> x 0))
        z (every-pred f g)]
    (z 89/6 4/5)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (> (count x) 2))
        z (every-pred f g)]
    (z "Hola")))

(defn función-every-pred-17
  []
  (let [f (fn [x] (coll? x))
        g (fn [x] (every? coll? x))
        h (fn [x] (every? empty? x))
        z (every-pred f g h)]
    (z ['() (list) (vector) [] #{} {}])))

(defn función-every-pred-18
  []
  (let [f (fn [x] (every? (fn [y] (> y 100)) x))
        g (fn [x] (every? (fn [y] (< y 1000)) x))
        h (fn [x] (every? integer? x))
        z (every-pred f g h)]
    (z (list 192 311 9103 121))))

(defn función-every-pred-19
  []
  (let [f (fn [x] (some rational? x))
        g (fn [x] (every? number? x))
        z (every-pred f g)]
    (z [12/5 4/7 8/3 123 90])))

(defn función-every-pred-20
  []
  (let [f (fn [x] (associative? x))
        g (fn [x] (distinct? x))
        z (every-pred f g)]
    (z '(1 2 4 1 3 2))))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

;;fnil
(defn función-fnil-1
  []
  (let [f (fn [x y] (* x y))
        z (fnil f 20)]
    (z nil 6)))

(defn función-fnil-2
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f 100)]
    (z nil 9)))

(defn función-fnil-3
  []
  (let [f (fn [x y z] (* (/ 100 x) (min y z)))
        z (fnil f 10 30)]
    (z nil nil 20)))

(defn función-fnil-4
  []
  (let [f (fn [x y] (max x y))
        z (fnil f 100)]
    (z nil 12)))

(defn función-fnil-5
  []
  (let [f (fn [x y] (range x y))
        z (fnil f 10 20)]
    (z nil nil)))

(defn función-fnil-6
  []
  (let [f (fn [x y z] (vector x y z))
        z (fnil f 12 8 6)]
    (z 10 nil 30)))

(defn función-fnil-7
  []
  (let [f (fn [x y] (vector (char x) (char y)))
        z (fnil f 65)]
    (z nil 80)))

(defn función-fnil-8
  []
  (let [f (fn [x] (rand x))
        z (fnil f 3)]
    (z nil)))

(defn función-fnil-9
  []
  (let [f (fn [x y] (= x y))
        z (fnil f "Hola")]
    (z nil "hola")))

(defn función-fnil-10
  []
  (let [f (fn [x y] (quot x y))
        z (fnil f 5)]
    (z nil 2)))

(defn función-fnil-11
  []
  (let [f (fn [x] (reverse x))
        z (fnil f "Alejandra")]
    (z nil)))

(defn función-fnil-12
  []
  (let [f (fn [x] (first x))
        z (fnil f [1 2 3])]
    (z [5 6 7])))

(defn función-fnil-13
  []
  (let [f (fn [x] (last x))
        z (fnil f '(98 76 54))]
    (z nil)))

(defn función-fnil-14
  []
  (let [f (fn [x y] (= x y))
        z (fnil f 70 34)]
    (z nil 70)))

(defn función-fnil-15
  []
  (let [f (fn [x y] (rem x y))
        z (fnil f 40)]
    (z nil 3)))

(defn función-fnil-16
  []
  (let [f (fn [x y] (if (odd? x) (* 10 y) (* 10 x)))
        z (fnil f 20)]
    (z nil 30)))

(defn función-fnil-17
  []
  (let [f (fn [x] (if (even? x) (dec x) (inc x)))
        z (fnil f 50)]
    (z nil)))

(defn función-fnil-18
  []
  (let [f (fn [x y] (/ x y))
        z (fnil f 90 12)]
    (z nil 45)))

(defn función-fnil-19
  []
  (let [f (fn [x y] (hash-map x y))
        z (fnil f "uno")]
    (z nil 23)))

(defn función-fnil-20
  []
  (let [f (fn [x] (float x))
        z (fnil f 12)]
    (z nil)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

;;juxt
(defn función-juxt-1
  []
  (let [f (fn [x] (* 20 x))
        g (fn [x] (/ x 100))
        z (juxt f g)]
    (z 100)))

(defn función-juxt-2
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (repeat 3 x))
        h (fn [x] (apply str x))
        z (juxt f g h)]
    (z [1 2 3 5])))

(defn función-juxt-3
  []
  (let [f (fn [x] (map inc x))
        g (fn [x] (map dec x))
        z (juxt f g)]
    (z [90 80 70 60])))

(defn función-juxt-4
  []
  (let [f (fn [x] (first x))
        g (fn [x] (filter odd? x))
        h (fn [x] (map char x))
        z (juxt f g h)]
    (z '(70 71 72 73 74))))

(defn función-juxt-5
  []
  (let [f (fn [x] (+ 65 x))
        g (fn [x] (char x))
        h (fn [x] (str x))
        z (juxt f g h)]
    (z 90)))

(defn función-juxt-6
  []
  (let [f (fn [x] (map float x))
        g (fn [x] (filter (fn [y] (and (>= y 97) (<= y 122))) x))
        h (fn [x] (map char x))
        z (juxt f g h)]
    (z [92 233 38 122 311])))

(defn función-juxt-7
  []
  (let [f (fn [x] (+ x 10))
        g (fn [x] (* x 10))
        h (fn [x] (- x 10))
        i (fn [x] (/ x 10))
        z (juxt f g h i)]
    (z 30)))

(defn función-juxt-8
  []
  (let [f (fn [x] (take 1 x))
        g (fn [x] (map inc x))
        z (juxt f g)]
    (z '(10 20 30 50))))

(defn función-juxt-9
  []
  (let [f (fn [x] (+ x 120))
        g (fn [x] (* x x))
        h (fn [x] (/ x 100))
        i (fn [x] (double x))
        z (juxt f g h i)]
    (z 90.1)))

(defn función-juxt-10
  []
  (let [f (fn [x] (filter odd? x))
        g (fn [x] (map char x))
        h (fn [x] (apply str x))
        z (juxt f g h)]
    (z [69 122 92 233])))

(defn función-juxt-11
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 100 x))
        z (juxt f g)]
    (z 9012)))

(defn función-juxt-12
  []
  (let [f (fn [x] (map char x))
        g (fn [x] (map int x))
        h (fn [x] (reverse x))
        z (juxt f g h)]
    (z "Yazmin")))

(defn función-juxt-13
  []
  (let [f (fn [x] (str x))
        g (fn [x] (quot x 100))
        h (fn [x] (repeat 2 x))
        z (juxt f g h)]
    (z 913834)))

(defn función-juxt-14
  []
  (let [f (fn [x] (filter even? x))
        g (fn [x] (into #{} x))
        z (juxt f g)]
    (z (range 10))))

(defn función-juxt-15
  []
  (let [f (fn [x] (into '() x))
        g (fn [x] (map neg? x))
        z (juxt f g)]
    (z (range 70 81))))

(defn función-juxt-16
  []
  (let [f (fn [x] (apply max x))
        g (fn [x] (first x))
        h (fn [x] (take-last 6 x))
        i (fn [x] (reduce + x))
        z (juxt f g h i)]
    (z '(19 23 421 4193 123 12 4 0))))

(defn función-juxt-17
  []
  (let [f (fn [x] (into #{} x))
        g (fn [x] (take 2 x))
        h (fn [x] (sort x))
        z (juxt f g h)]
    (z [0 321 423 4 1 3 23 5])))

(defn función-juxt-18
  []
  (let [f (fn [x] (map float x))
        g (fn [x] (map char x))
        z (juxt f g)]
    (z (list 92 90 42 411 233))))

(defn función-juxt-19
  []
  (let [f (fn [x] (+ x x x x))
        g (fn [x] (* x x x))
        h (fn [x] (- x x))
        i (fn [x] (/ x))
        z (juxt f g h i)]
    (z 10)))

(defn función-juxt-20
  []
  (let [f (fn [x] (quot x 10))
        g (fn [x] (rem x 10))
        h (fn [x] (mod x 10))
        z (juxt f g h)]
    (z 16)))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

;;partial
(defn función-partial-1
  []
  (let [f (fn [x y] (* x y))
        z (partial f)]
    (z 20 10)))

(defn función-partial-2
  []
  (let [f (fn [x y z] (vector x y z))
        z (partial f "uno" "dos")]
    (z 3)))

(defn función-partial-3
  []
  (let [f (fn [x] (range x))
        z (partial f 10)]
    (z)))

(defn función-partial-4
  []
  (let [f (fn [x] (rand x))
        z (partial f)]
    (z 100)))

(defn función-partial-5
  []
  (let [f (fn [x y z] (min x y z))
        z (partial f 19 33)]
    (z 0)))

(defn función-partial-6
  []
  (let [f (fn [x y] (mod x y))
        z (partial f 120)]
    (z 10)))

(defn función-partial-7
  []
  (let [f (fn [x] (count x))
        z (partial f)]
    (z [0 3 1 5 2 4 12 6 8])))

(defn función-partial-8
  []
  (let [f (fn [x] (map char x))
        z (partial f [70 75 80 85])]
    (z)))

(defn función-partial-9
  []
  (let [f (fn [x y z] (list x y z))
        z (partial f "w" \b)]
    (z 90.1)))

(defn función-partial-10
  []
  (let [f (fn [x y] (rem x y))
        z (partial f 90)]
    (z 100)))

(defn función-partial-11
  []
  (let [f (fn [x] (inc x))
        z (partial f 5)]
    (z)))

(defn función-partial-12
  []
  (let [f (fn [x] (dec x))
        z (partial f)]
    (z 92)))

(defn función-partial-13
  []
  (let [f (fn [x y] (str "Hola " x " " y))
        z (partial f "Yazmin")]
    (z "Alejandra")))

(defn función-partial-14
  []
  (let [f (fn [x y] (take x y))
        z (partial f 4)]
    (z [1 2 3 4 "s" "i" 0 "no"])))

(defn función-partial-15
  []
  (let [f (fn [x y] (take-last x y))
        z (partial f)]
    (z 2 '(0 1 3 5 12 4 2 53))))

(defn función-partial-16
  []
  (let [f (fn [x y] (x y))
        z (partial f odd?)]
    (z 900)))

(defn función-partial-17
  []
  (let [f (fn [x y z] (x y z))
        z (partial f repeat)]
    (z 2 "hola")))

(defn función-partial-18
  []
  (let [f (fn [x] (reverse x))
        z (partial f "PLF")]
    (z)))

(defn función-partial-19
  []
  (let [f (fn [x y] (remove x y))
        z (partial f neg?)]
    (z [1 -2 -3 4 -5 6])))

(defn función-partial-20
  []
  (let [f (fn [x] (distinct x))
        z (partial f)]
    (z [1 3 2 2 4 6 7 9 9 2])))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

;;some-fn
(defn función-some-fn-1
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (associative? x))
        z (some-fn f g)]
    (z [#{} [] '() "some" 12])))

(defn función-some-fn-2
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (distinct? x))
        z (some-fn f g)]
    (z '([1 2 3] [] (vector 0 5 7) 233))))

(defn función-some-fn-3
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (rational? x))
        z (some-fn f g)]
    (z (list "a" "b" :c))))

(defn función-some-fn-4
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (empty? x))
        h (fn [x] (coll? x))
        z (some-fn f g h)]
    (z #{{} [] 12.1})))

(defn función-some-fn-5
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (true? x))
        z (some-fn f g)]
    (z '((> 2 3) (= 1 22) false))))

(defn función-some-fn-6
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (pos? x))
        z (some-fn f g)]
    (z 12 12.2)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z 1 2 4 5 6)))

(defn función-some-fn-8
  []
  (let [g (fn [x] (> x 10))
        h (fn [x] (< x 20))
        z (some-fn g h)]
    (z 0)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (> (count x) 2))
        z (some-fn f g)]
    (z [1])))

(defn función-some-fn-10
  []
  (let [f (fn [x] (zero? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z 0)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (> x 100))
        g (fn [x] (< x 1000))
        h (fn [x] (integer? x))
        z (some-fn f g h)]
    (z 1000)))

(defn función-some-fn-12
  []
  (let [f (fn [x] (rational? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z "medio")))

(defn función-some-fn-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (ratio? x))
        z (some-fn f g)]
    (z 2/5 9/4)))

(defn función-some-fn-14
  []
  (let [f (fn [x] (ratio? x))
        g (fn [x] (> x 0))
        z (some-fn f g)]
    (z 9 12)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (= 6 (count x)))
        g (fn [x] (string? x))
        z (some-fn f g)]
    (z "Yazmin")))

(defn función-some-fn-16
  []
  (let [f (fn [x] (associative? x))
        g (fn [x] (distinct? x))
        z (some-fn f g)]
    (z [1 2 4 6 7 8 8 9])))

(defn función-some-fn-17
  []
  (let [f (fn [x] (coll? x))
        g (fn [x] (empty? x))
        z (some-fn f g)]
    (z "doce")))

(defn función-some-fn-18
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (string? x))
        z (some-fn f g)]
    (z [\c \f])))

(defn función-some-fn-19
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z '("uno" "dos" "tres"))))

(defn función-some-fn-20
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z [\c \g 'u])))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)